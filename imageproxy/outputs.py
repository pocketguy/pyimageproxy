import typing
from io import BytesIO

import attr
from PIL import Image

from imageproxy.casts import not_negative_int_cast, parse_schema


@attr.s(auto_attribs=True)
class Output:
    format: typing.ClassVar[str] = None
    mime: typing.ClassVar[str] = None
    schema: typing.ClassVar[dict] = {}

    @property
    def mimetype(self):
        return Image.MIME.get(self.format)

    @classmethod
    async def from_params(cls, params):
        positionals, flags, choices = parse_schema(cls.schema, params)
        return cls(*positionals, **flags, **choices)

    @staticmethod
    async def save_default(image: Image.Image, format: str) -> typing.Tuple[bytes, str]:
        with BytesIO() as bio:
            image.save(bio, format=format)
            return bio.getvalue(), Image.MIME[format.upper()]

    async def save(self, image: Image.Image) -> typing.Tuple[bytes, str]:
        with BytesIO() as bio:
            image.save(bio, format=self.format, **attr.asdict(self))
            return bio.getvalue(), self.mime or Image.MIME[self.format.upper()]


@attr.s(auto_attribs=True)
class JPEGOutput(Output):
    format: typing.ClassVar[str] = 'JPEG'
    schema: typing.ClassVar[dict] = {
        'flags': ['progressive'],
        'choices': {
            'quality': not_negative_int_cast
        }
    }
    progressive: bool = False
    quality: int = 75

    async def save(self, image: Image.Image) -> typing.Tuple[bytes, str]:
        image = image.convert('RGB')
        return await super().save(image)


@attr.s(auto_attribs=True)
class JPEG2000Output(Output):
    format: typing.ClassVar[str] = 'JPEG2000'
    schema: typing.ClassVar[dict] = {}


@attr.s(auto_attribs=True)
class BMPOutput(Output):
    format: typing.ClassVar[str] = 'BMP'
    schema: typing.ClassVar[dict] = {}


@attr.s(auto_attribs=True)
class EPSOutput(Output):
    format: typing.ClassVar[str] = 'EPS'
    schema: typing.ClassVar[dict] = {}

    async def save(self, image: Image.Image) -> typing.Tuple[bytes, str]:
        image = image.convert('RGB')
        return await super().save(image)


@attr.s(auto_attribs=True)
class ICOOutput(Output):
    format: typing.ClassVar[str] = 'ICO'
    mime: typing.ClassVar[str] = 'image/vnd.microsoft.icon'
    schema: typing.ClassVar[dict] = {}


@attr.s(auto_attribs=True)
class PNGOutput(Output):
    format: typing.ClassVar[str] = 'PNG'
    schema: typing.ClassVar[dict] = {
        'flags': ['optimize']
    }
    optimize: bool = False


@attr.s(auto_attribs=True)
class WebPOutput(Output):
    format: typing.ClassVar[str] = 'WEBP'
    schema: typing.ClassVar[dict] = {
        'flags': ['lossless'],
        'choices': {
            'quality': not_negative_int_cast,
            'method': not_negative_int_cast
        }
    }
    lossless: bool = False
    quality: int = 80
    method: int = 0

    async def save(self, image: Image.Image) -> typing.Tuple[bytes, str]:
        return await super().save(image)


@attr.s(auto_attribs=True)
class PDFOutput(Output):
    format: typing.ClassVar[str] = 'PDF'
    schema: typing.ClassVar[dict] = {}

    async def save(self, image: Image.Image) -> typing.Tuple[bytes, str]:
        image = image.convert('RGB')
        return await super().save(image)

