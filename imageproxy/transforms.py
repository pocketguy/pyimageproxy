import typing

import attr
from PIL import Image

from imageproxy.casts import not_negative_int_cast, int_cast, resample_cast, parse_schema


@attr.s(auto_attribs=True)
class Transform:
    schema: typing.ClassVar[dict] = {}

    async def apply(self, image: Image.Image):
        raise NotImplementedError()

    @classmethod
    async def from_params(cls, params: typing.List[str]):
        positionals, flags, choices = parse_schema(cls.schema, params)
        return cls(*positionals, **flags, **choices)


@attr.s(auto_attribs=True)
class RotateTransform(Transform):
    schema: typing.ClassVar[dict] = {
        'positionals': [int_cast],
        'flags': ['expand'],
        'choices': {
            'resample': resample_cast
        }
    }
    angle: int
    resample: int = Image.NEAREST
    expand: bool = False

    async def apply(self, image: Image.Image):
        return image.rotate(self.angle, self.resample, self.expand)


@attr.s(auto_attribs=True)
class ResizeTransform(Transform):
    schema: typing.ClassVar[dict] = {
        'positionals': [
            not_negative_int_cast,
            not_negative_int_cast
        ],
        'choices': {
            'resample': resample_cast
        }
    }
    width: int
    height: int
    resample: int = Image.NEAREST

    async def apply(self, image: Image.Image):
        # TODO: make max resize with same as image width
        if self.width and self.height:
            # doesn't keep aspect ratio
            final_width = self.width
            final_height = self.height
        elif self.width:
            # height not specified
            final_width = self.width
            scale = final_width / image.width
            final_height = int(image.height * scale)
        elif self.height:
            # width not specified
            final_height = self.height
            scale = final_height / image.height
            final_width = int(image.width * scale)
        else:
            return image
        return image.resize((final_width, final_height), self.resample)
