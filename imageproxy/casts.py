import typing

from PIL import Image

from imageproxy.exceptions import BadTransformParamsException


def parse_schema(schema: dict, params: typing.List[str]) -> typing.Tuple[typing.List, typing.Dict, typing.Dict]:
    schema_positionals = schema.get('positionals', [])
    schema_flags = schema.get('flags', [])
    schema_choices = schema.get('choices', {})
    positionals = []
    flags = {}
    choices = {}
    for idx, param in enumerate(params):
        if idx < len(schema_positionals):
            cast_method = schema_positionals[idx]
            positional = cast_method(param)
            positionals.append(positional)
            continue
        if param in schema_flags:
            flags[param] = True
            continue
        for schema_choice_name, schema_choice_cast in schema_choices.items():
            left_part = schema_choice_name + '='
            if param.startswith(left_part):
                param_value = param.replace(left_part, '')
                choices[schema_choice_name] = schema_choice_cast(param_value)
                break
    return positionals, flags, choices


def int_cast(x):
    try:
        return int(x)
    except ValueError as e:
        raise BadTransformParamsException(f'Can not cast "{x}" to not negative integer') from e


def not_negative_int_cast(x):
    intval = int_cast(x)
    if intval < 0:
        raise BadTransformParamsException(f'Got value "{intval}", but wanted positive or 0')
    return intval


RESAMPLE_OPTIONS = {
    'nearest': Image.NEAREST,
    'bilinear': Image.BILINEAR,
    'bicubic': Image.BICUBIC
}


def resample_cast(x):
    try:
        return RESAMPLE_OPTIONS[x]
    except KeyError as e:
        allowed = ', '.join(RESAMPLE_OPTIONS.keys())
        raise BadTransformParamsException(
            f'No such resample option: "{x}", (allowed: {allowed})'
        ) from e
