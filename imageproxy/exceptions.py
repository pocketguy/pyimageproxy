class ImageProxyException(Exception):
    pass


class BadTransformParamsException(ImageProxyException):
    pass


class OutputException(ImageProxyException):
    pass
