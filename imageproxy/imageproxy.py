import attr
import typing
from PIL import Image
from io import BytesIO

from imageproxy.outputs import Output
from imageproxy.transforms import Transform


@attr.s(auto_attribs=True)
class ImageProxy:
    image: Image.Image
    output: Output
    transforms: typing.Iterable[Transform] = attr.Factory(list)
    input_format: str = attr.ib(init=False)

    async def apply_transforms(self):
        for transform in self.transforms:
            self.image = await transform.apply(self.image)

    async def apply_output(self) -> typing.Tuple[bytes, str]:
        if not self.output or not self.output.format:
            return await Output.save_default(self.image, self.input_format)
        else:
            return await self.output.save(self.image)

    @staticmethod
    async def do_proxy(
            image_bytes: bytes,
            transforms: typing.Iterable[Transform],
            output: Output = None
    ) -> typing.Tuple[bytes, str]:
        with BytesIO(image_bytes) as bio:
            image = Image.open(bio)
            image.load()
        self = ImageProxy(image, output, transforms)
        self.input_format = image.format
        await self.apply_transforms()
        return await self.apply_output()
