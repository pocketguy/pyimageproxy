FROM python:3.7-alpine

WORKDIR /usr/src/app

COPY requirements ./requirements
COPY requirements.txt .

ENV CC cc -mavx2

RUN apk add --no-cache \
    build-base \
    jpeg-dev \
    zlib-dev \
    libwebp-dev \
    openjpeg-dev \
    && pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000

CMD ["gunicorn", "-c", "gunicorn_config.py", "app:create_app"]