export CC=cc -mavx2

all: install

install-venv:
	python3 -m venv --copies venv

install: install-venv
	venv/bin/pip install --no-cache-dir -r requirements.txt

install-dev: install-venv
	venv/bin/pip install --no-cache-dir -r requirements/dev.txt

install-release: install-venv
	venv/bin/pip install --no-cache-dir -r requirements/release.txt

clean:
	rm -rf venv
	find . -name __pycache__ -type d -exec rm -r "{}" \;
