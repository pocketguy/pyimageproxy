from aiohttp import web
from api import api
import os


async def create_app() -> web.Application:
    app = web.Application()
    app.add_routes([web.get('/', api.handle)])
    if os.getenv('DEBUG', '').lower() in ('1', 'true'):
        import aiohttp_debugtoolbar
        aiohttp_debugtoolbar.setup(app)
    return app


if __name__ == '__main__':
    import sys
    port = sys.argv[1]
    app = create_app()
    web.run_app(app, port=port, access_log=None)
