import argparse
import os

import attr
import docker

client = docker.from_env()


@attr.s(auto_attribs=True)
class SemVer:
    major: int
    minor: int
    patch: int

    @classmethod
    def from_string(cls, string):
        def fail(reason, exc=None):
            full_reason = f'Can not parse {string} into semver: {reason}'
            raise Exception(full_reason) from exc

        parts = string.split('.')
        if len(parts) != 3:
            fail('there are not exactly 3 parts')

        int_parts = []

        for part in parts:
            try:
                int_part = int(part)
                if int_part < 0:
                    fail(f'part "{int_part}" is lower then 0')
                int_parts.append(int_part)
            except ValueError as e:
                fail(f'"{part}" is not integer', e)
        return cls(*int_parts)

    @property
    def str_major(self):
        return f'{self.major}'

    @property
    def str_minor(self):
        return f'{self.major}.{self.minor}'

    @property
    def str_patch(self):
        return f'{self.major}.{self.minor}.{self.patch}'

    def __str__(self):
        return self.str_patch


def release_to_docker_hub(version: SemVer, docker_image_name: str):
    print(f'Building image: {docker_image_name}')
    image, log = client.images.build(path=os.getcwd())
    client.images.push(docker_image_name,)
    print(f'Successfully built image: {docker_image_name}')
    for tag in ['latest', version.str_major, version.str_minor, version.str_patch]:
        image.tag(docker_image_name, tag=tag)
        client.images.push(docker_image_name, tag=tag)
        print(f'Pushing {tag} to docker hub')


def main():
    parser = argparse.ArgumentParser(description='Release script')
    parser.add_argument('version', help='semantic version like 1.12.2', type=SemVer.from_string)
    parser.add_argument('--docker_image_name', help='docker image name', default='pocketguy/pyimageproxy')
    args = parser.parse_args()
    version = args.version
    docker_image_name = args.docker_image_name
    release_to_docker_hub(version, docker_image_name)


if __name__ == '__main__':
    main()
