import typing

from aiohttp import ClientSession, web, InvalidURL

from api.exceptions import FetchImageException, NoSuchTransformException, NoSuchOutputException
from imageproxy import transforms, imageproxy, exceptions, outputs


async def fetch_image(url) -> bytes:
    async with ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status != 200:
                raise FetchImageException(f'Request to {url} ended with status code {resp.status}')
            return await resp.read()

TRANSFORMS = {
    'rotate': transforms.RotateTransform,
    'resize': transforms.ResizeTransform
}

OUTPUTS = {
    'bmp': outputs.BMPOutput,
    'eps': outputs.EPSOutput,
    'ico': outputs.ICOOutput,
    'jpeg2000': outputs.JPEG2000Output,  # must be before jpeg
    'jpeg': outputs.JPEGOutput,
    'jpg': outputs.JPEGOutput,
    'pdf': outputs.PDFOutput,
    'png': outputs.PNGOutput,
    'webp': outputs.WebPOutput,
}


async def make_transforms(transforms_query: str):
    if not transforms_query:
        return []
    transforms_strings = transforms_query.split(',')
    ts = []
    for transform_string in transforms_strings:
        transform_tokens = transform_string.split(':')
        method = transform_tokens[0]
        params = transform_tokens[1:]
        if method not in TRANSFORMS:
            raise NoSuchTransformException(f'Transform not found: {method}')
        transform = await TRANSFORMS[method].from_params(params)
        ts.append(transform)
    return ts


async def make_output(output_query: str) -> typing.Optional[outputs.Output]:
    if not output_query:
        return None
    output_params = output_query.split(':')
    output_format = output_params[0]
    output_params = output_params[1:]
    for supported_output in OUTPUTS:
        if supported_output == output_format:
            return await OUTPUTS[supported_output].from_params(output_params)
    raise NoSuchOutputException(f'No such format: {output_format}')


async def handle(request: web.Request) -> web.Response:
    src = request.query.get('from')
    transforms_query = request.query.get('with')
    output_query = request.query.get('to')
    if not src:
        return web.HTTPBadRequest(text='Please, specify src query param')
    try:
        image = await fetch_image(src)
    except InvalidURL:
        return web.HTTPBadRequest(text=f'Invalid from: {src}')
    except FetchImageException as e:
        return web.HTTPBadRequest(text=str(e))
    try:
        transforms = await make_transforms(transforms_query)
    except (exceptions.BadTransformParamsException, NoSuchTransformException) as e:
        return web.HTTPBadRequest(text=str(e))
    try:
        output = await make_output(output_query)
    except NoSuchOutputException as e:
        return web.HTTPBadRequest(text=str(e))
    body, mimetype = await imageproxy.ImageProxy.do_proxy(image, transforms, output)
    return web.Response(body=body, content_type=mimetype)
