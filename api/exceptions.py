class ApiException(Exception):
    pass


class FetchImageException(ApiException):
    pass


class NoSuchTransformException(ApiException):
    pass


class NoSuchOutputException(ApiException):
    pass
